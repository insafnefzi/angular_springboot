import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpParams } from '@angular/common/http';
import { AppSettings } from 'src/app/settings/app.settings';
import { Anime } from 'src/app/models/anime';

@Injectable({
  providedIn: 'root'
})
export class AnimeService {

  constructor(private http: HttpClient) { }
  findAllAnime(){
    return this.http.get<Anime>(AppSettings.APP_URL+"/animes/");
  }
  findAllUserAnime(idUser: number) {
    return this.http.get<Anime[]>(AppSettings.APP_URL + "/animes/all/" + idUser);
  }
  findAnimeById (idAnime : number){
    return this.http.get<Anime>(AppSettings.APP_URL+"/animes/"+idAnime);
  }
  saveAnime(anime: Anime){
    this.http.post<Anime>(AppSettings.APP_URL+"/animes/",anime);
  }
  shareAnime(idAnime: number, isShared: boolean){
    return this.http.get<Anime>(AppSettings.APP_URL+"/animes/share/"+idAnime+"/"+isShared);
  }
  deleteCharacter(idAnime: number) {
    return this.http.delete<Anime>(AppSettings.APP_URL + "/animes/" + idAnime);
  }

}
