export class Anime{
        idAnime: number;
        animeName:string;
        category:string;
        strength:string;
        shared: boolean;
        idOwner: number;
        legend: string;
}