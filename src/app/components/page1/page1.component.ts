import { Component, OnInit } from '@angular/core';
import { Page1Service } from 'src/app/services/page1/page1.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
somme : number;
  constructor(private service: Page1Service) { }

  ngOnInit() {
this.somme = this.service.somme(1,2);
  }

}
